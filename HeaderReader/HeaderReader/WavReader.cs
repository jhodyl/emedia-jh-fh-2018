﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using MathNet.Numerics;

namespace HeaderReader
{
    public class WavReader
    {
        //RIFF
        public string ChunkID;
        public int ChunkSize;
        public float ChunkSizeMb;
        public string ChunkFormat;

        //fmt
        public string SubchunkID;
        public int SubchunkSize;
        public int AudioFormat;
        public int NumberOfChannels;
        public int SamplesPerSecond;
        public int BytesPerSecond;
        public int BlockAlign;
        public int BitsPerSample;
        public int ExtraParameters;

        //data
        public string Subchunk2ID;
        public int Subchunk2Size;

        public string PrintHeaderInfo()
        {
            string answers = $"ChunkID: {ChunkID}\nChunkSize: {ChunkSize}\nChunkSizeMb: {ChunkSizeMb}\nChunkFormat: {ChunkFormat}\n\n" +
                $"SubchunkID: {SubchunkID}\nSubchunkSize: {SubchunkSize}\nAudioFormat: {AudioFormat}\nNumberOfChannels: {NumberOfChannels}\n" +
                $"SamplesPerSecond: {SamplesPerSecond}\nBytesPerSecond: {BytesPerSecond}\nBlockAlign: {BlockAlign}\nBitsPerSample: {BitsPerSample}\n" +
                $"ExtraParameters: {ExtraParameters}";
            return answers;
        }
        public double[] Samples;
        public void ReadHeader(string FileName)
        {
            using (BinaryReader reader = new BinaryReader(File.Open(FileName, FileMode.Open)))
            {
                ChunkID = new string(reader.ReadChars(4));
                ChunkSize = reader.ReadInt32();
                ChunkSizeMb = (ChunkSize/1024f)/1024f;
                ChunkFormat = new string(reader.ReadChars(4));
                SubchunkID = new string(reader.ReadChars(4));
                SubchunkSize = reader.ReadInt32();
                AudioFormat = reader.ReadInt16();
                NumberOfChannels = reader.ReadInt16();
                SamplesPerSecond = reader.ReadInt32();
                BytesPerSecond = reader.ReadInt32();
                BlockAlign = reader.ReadInt16();
                BitsPerSample = reader.ReadInt16();
                Subchunk2ID = new string(reader.ReadChars(4));
                Subchunk2Size = reader.ReadInt32();

                Samples = new double[1024 * BitsPerSample / 8];

                double tmp;
                for (int i = 0; i < Samples.Length; i++)
                {
                    switch(BitsPerSample)
                    {
                        case 8:
                            tmp = reader.ReadInt16();
                            Samples[i] = tmp;
                            break;
                        case 16:
                            tmp = reader.ReadInt16();
                            Samples[i] = tmp;
                            break;
                        case 32:
                            tmp = reader.ReadInt32();
                            Samples[i] = tmp;
                            break;
                    }
                }

            }

            
        }
    }
}
