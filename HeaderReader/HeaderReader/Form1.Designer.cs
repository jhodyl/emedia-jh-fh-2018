﻿namespace HeaderReader
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.WybierzSciezkePliku = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.FftChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            ((System.ComponentModel.ISupportInitialize)(this.FftChart)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(285, 114);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Wykonaj!";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Wykonaj_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(219, 45);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(216, 20);
            this.textBox1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(100, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Podaj ścieżkę pliku:";
            // 
            // WybierzSciezkePliku
            // 
            this.WybierzSciezkePliku.Location = new System.Drawing.Point(441, 40);
            this.WybierzSciezkePliku.Name = "WybierzSciezkePliku";
            this.WybierzSciezkePliku.Size = new System.Drawing.Size(75, 29);
            this.WybierzSciezkePliku.TabIndex = 3;
            this.WybierzSciezkePliku.Text = "Wybierz...";
            this.WybierzSciezkePliku.UseVisualStyleBackColor = true;
            this.WybierzSciezkePliku.Click += new System.EventHandler(this.WybierzSciezkePliku_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(420, 104);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(96, 43);
            this.button2.TabIndex = 4;
            this.button2.Text = "Wyświetl szczegóły pliku";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Details_Click);
            // 
            // FftChart
            // 
            this.FftChart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            chartArea1.AxisX.LabelAutoFitMaxFontSize = 8;
            chartArea1.AxisX.LabelStyle.Enabled = false;
            chartArea1.AxisY.LabelStyle.Enabled = false;
            chartArea1.Name = "ChartArea1";
            this.FftChart.ChartAreas.Add(chartArea1);
            this.FftChart.Location = new System.Drawing.Point(52, 153);
            this.FftChart.Name = "FftChart";
            series1.ChartArea = "ChartArea1";
            series1.Name = "FFT";
            series1.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            this.FftChart.Series.Add(series1);
            this.FftChart.Size = new System.Drawing.Size(536, 162);
            this.FftChart.TabIndex = 5;
            this.FftChart.Text = "FftChart";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(656, 327);
            this.Controls.Add(this.FftChart);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.WybierzSciezkePliku);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.FftChart)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button WybierzSciezkePliku;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataVisualization.Charting.Chart FftChart;
    }
}

