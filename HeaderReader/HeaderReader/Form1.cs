﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace HeaderReader
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        
        private void WybierzSciezkePliku_Click(object sender, EventArgs e)
        {
            Stream myStream = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.InitialDirectory = "C:\\";
            openFileDialog1.Filter = "wav files (*.wav)|*.wav|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if (Path.GetExtension(openFileDialog1.FileName) == ".wav")
                {
                    textBox1.Text = openFileDialog1.FileName;
                } else
                {
                    MessageBox.Show("Nie wybrano właściwego rozszerzenia pliku (.wav)!");
                }
                
            }
            
        }
        
        private void Form1_Load(object sender, EventArgs e)
        {
            this.Text = "Wyświetlanie widma dla wybranego pliku";
        }

        private void Wykonaj_Click(object sender, EventArgs e)
        {
            
            if (string.IsNullOrWhiteSpace(textBox1.Text))
            {
                MessageBox.Show("Nie podano ścieżki do pliku!");
            } else
            {
                if (File.Exists(textBox1.Text)) {
                SampleMaker.MakeSamples(textBox1.Text);
                //Console.ReadKey();
                double[] wynik = FFTMaker.FFT(SampleMaker.wread.Samples);
                    if (FftChart.Series[0].Points.Count != 0) // jesli jest juz chart a ktos chce drugi raz to go czysci a potem dopiero zapisuje do niego
                    {
                        FftChart.Series[0].Points.Clear();
                        FftChart.Titles.Clear();
                    }
                    for (int i = 0; i < wynik.Length / 2; i++)
                    {
                        FftChart.Series[0].Points.AddY(wynik[i]);
                    }
                    FftChart.Titles.Add(textBox1.Text);
                    MessageBox.Show("Wykonano!");

                } else
                {
                    MessageBox.Show("Plik o podanej ścieżce nie istnieje!");
                }
               
            }
           
            
        }
        
        private void Details_Click(object sender, EventArgs e)
        {   
            if (SampleMaker.wread.ChunkSize != 0)
            {
                MessageBox.Show(SampleMaker.wread.PrintHeaderInfo());
            } else
            {
                MessageBox.Show("Wgraj plik!");
            }
        }

    }
}
